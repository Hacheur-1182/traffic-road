import 'package:flutter/material.dart';

import 'package:get/get.dart';

import 'app/routes/app_pages.dart';

void main() {
  runApp(
    GetMaterialApp(
      title: "Traffic Road",
      debugShowCheckedModeBanner: false,
      initialRoute: AppPages.LOGIN,
      getPages: AppPages.routes,
    ),
  );
}
