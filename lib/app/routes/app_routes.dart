part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const LOGIN = _Paths.LOGIN;
  static const ROAD_TRAFFIC = _Paths.ROAD_TRAFFIC;
  static const ERROR_CONNECTION = _Paths.ERROR_CONNECTION;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const LOGIN = '/login';
  static const ROAD_TRAFFIC = '/road-traffic';
  static const ERROR_CONNECTION = '/error-connection';
}
