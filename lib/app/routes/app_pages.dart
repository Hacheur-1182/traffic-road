import 'package:get/get.dart';

import '../modules/error_connection/bindings/error_connection_binding.dart';
import '../modules/error_connection/views/error_connection_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/login/bindings/login_bindings.dart';
import '../modules/login/views/login_view.dart';
import '../modules/road_traffic/bindings/road_traffic_binding.dart';
import '../modules/road_traffic/views/road_traffic_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.HOME;
  static const LOGIN = Routes.LOGIN;
  static const ROAD_TRAFFIC = Routes.ROAD_TRAFFIC;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => const HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => const LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.ROAD_TRAFFIC,
      page: () => const RoadTrafficView(),
      binding: RoadTrafficBinding(),
    ),
    GetPage(
      name: _Paths.ERROR_CONNECTION,
      page: () => const ErrorConnectionView(),
      binding: ErrorConnectionBinding(),
    ),
  ];
}
