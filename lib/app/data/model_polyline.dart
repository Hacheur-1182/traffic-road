import 'dart:convert';

PolylineModel polylineModelFromJson(String str) =>
    PolylineModel.fromJson(json.decode(str));

String polylineModelToJson(PolylineModel data) => json.encode(data.toJson());

class PolylineModel {
  List<RoadTraffic> routes;

  PolylineModel({
    required this.routes,
  });

  factory PolylineModel.fromJson(Map<String, dynamic> json) => PolylineModel(
        routes: List<RoadTraffic>.from(
            json["routes"].map((x) => RoadTraffic.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "routes": List<dynamic>.from(routes.map((x) => x.toJson())),
      };
}

class RoadTraffic {
  int distanceMeters;
  String duration;
  PolylineDraw polyline;
  List<String> routeLabels;

  RoadTraffic({
    required this.distanceMeters,
    required this.duration,
    required this.polyline,
    required this.routeLabels,
  });

  factory RoadTraffic.fromJson(Map<String, dynamic> json) => RoadTraffic(
        distanceMeters: json["distanceMeters"],
        duration: json["duration"],
        polyline: PolylineDraw.fromJson(json["polyline"]),
        routeLabels: List<String>.from(json["routeLabels"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "distanceMeters": distanceMeters,
        "duration": duration,
        "polyline": polyline.toJson(),
        "routeLabels": List<dynamic>.from(routeLabels.map((x) => x)),
      };
}

class PolylineDraw {
  String encodedPolyline;

  PolylineDraw({
    required this.encodedPolyline,
  });

  factory PolylineDraw.fromJson(Map<String, dynamic> json) => PolylineDraw(
        encodedPolyline: json["encodedPolyline"],
      );

  Map<String, dynamic> toJson() => {
        "encodedPolyline": encodedPolyline,
      };
}
