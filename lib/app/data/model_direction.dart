import 'dart:convert';

DirectionModel directionModelFromJson(String str) =>
    DirectionModel.fromJson(json.decode(str));

String directionModelToJson(DirectionModel data) => json.encode(data.toJson());

class DirectionModel {
  List<GeocodedWaypoint> geocodedWaypoints;
  List<RoadDirection> routes;
  String status;

  DirectionModel({
    required this.geocodedWaypoints,
    required this.routes,
    required this.status,
  });

  factory DirectionModel.fromJson(Map<String, dynamic> json) => DirectionModel(
        geocodedWaypoints: List<GeocodedWaypoint>.from(
          json["geocoded_waypoints"].map(
            (x) => GeocodedWaypoint.fromJson(x),
          ),
        ),
        routes: List<RoadDirection>.from(
          json["routes"].map(
            (x) => RoadDirection.fromJson(x),
          ),
        ),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "geocoded_waypoints":
            List<dynamic>.from(geocodedWaypoints.map((x) => x.toJson())),
        "routes": List<dynamic>.from(routes.map((x) => x.toJson())),
        "status": status,
      };
}

class GeocodedWaypoint {
  String geocoderStatus;
  String placeId;
  List<String> types;

  GeocodedWaypoint({
    required this.geocoderStatus,
    required this.placeId,
    required this.types,
  });

  factory GeocodedWaypoint.fromJson(Map<String, dynamic> json) =>
      GeocodedWaypoint(
        geocoderStatus: json["geocoder_status"],
        placeId: json["place_id"],
        types: List<String>.from(json["types"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "geocoder_status": geocoderStatus,
        "place_id": placeId,
        "types": List<dynamic>.from(types.map((x) => x)),
      };
}

class RoadDirection {
  BoundsViewPort bounds;
  String copyrights;
  List<LegDirection> legs;
  PolylineDirection overviewPolyline;
  String summary;
  List<dynamic> warnings;
  List<dynamic> waypointOrder;

  RoadDirection({
    required this.bounds,
    required this.copyrights,
    required this.legs,
    required this.overviewPolyline,
    required this.summary,
    required this.warnings,
    required this.waypointOrder,
  });

  factory RoadDirection.fromJson(Map<String, dynamic> json) => RoadDirection(
        bounds: BoundsViewPort.fromJson(json["bounds"]),
        copyrights: json["copyrights"],
        legs: List<LegDirection>.from(
          json["legs"].map(
            (x) => LegDirection.fromJson(x),
          ),
        ),
        overviewPolyline: PolylineDirection.fromJson(json["overview_polyline"]),
        summary: json["summary"],
        warnings: List<dynamic>.from(json["warnings"].map((x) => x)),
        waypointOrder: List<dynamic>.from(json["waypoint_order"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "bounds": bounds.toJson(),
        "copyrights": copyrights,
        "legs": List<dynamic>.from(legs.map((x) => x.toJson())),
        "overview_polyline": overviewPolyline.toJson(),
        "summary": summary,
        "warnings": List<dynamic>.from(warnings.map((x) => x)),
        "waypoint_order": List<dynamic>.from(waypointOrder.map((x) => x)),
      };
}

class BoundsViewPort {
  Northeast northeast;
  Northeast southwest;

  BoundsViewPort({
    required this.northeast,
    required this.southwest,
  });

  factory BoundsViewPort.fromJson(Map<String, dynamic> json) => BoundsViewPort(
        northeast: Northeast.fromJson(json["northeast"]),
        southwest: Northeast.fromJson(json["southwest"]),
      );

  Map<String, dynamic> toJson() => {
        "northeast": northeast.toJson(),
        "southwest": southwest.toJson(),
      };
}

class Northeast {
  double lat;
  double lng;

  Northeast({
    required this.lat,
    required this.lng,
  });

  factory Northeast.fromJson(Map<String, dynamic> json) => Northeast(
        lat: json["lat"]?.toDouble(),
        lng: json["lng"]?.toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "lat": lat,
        "lng": lng,
      };
}

class LegDirection {
  DistanceBetween distance;
  DistanceBetween duration;
  String endAddress;
  Northeast endLocation;
  String startAddress;
  Northeast startLocation;
  List<StepDirection> steps;
  List<dynamic> trafficSpeedEntry;
  List<dynamic> viaWaypoint;

  LegDirection({
    required this.distance,
    required this.duration,
    required this.endAddress,
    required this.endLocation,
    required this.startAddress,
    required this.startLocation,
    required this.steps,
    required this.trafficSpeedEntry,
    required this.viaWaypoint,
  });

  factory LegDirection.fromJson(Map<String, dynamic> json) => LegDirection(
        distance: DistanceBetween.fromJson(json["distance"]),
        duration: DistanceBetween.fromJson(json["duration"]),
        endAddress: json["end_address"],
        endLocation: Northeast.fromJson(json["end_location"]),
        startAddress: json["start_address"],
        startLocation: Northeast.fromJson(json["start_location"]),
        steps: List<StepDirection>.from(
          json["steps"].map(
            (x) => StepDirection.fromJson(x),
          ),
        ),
        trafficSpeedEntry:
            List<dynamic>.from(json["traffic_speed_entry"].map((x) => x)),
        viaWaypoint: List<dynamic>.from(json["via_waypoint"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "distance": distance.toJson(),
        "duration": duration.toJson(),
        "end_address": endAddress,
        "end_location": endLocation.toJson(),
        "start_address": startAddress,
        "start_location": startLocation.toJson(),
        "steps": List<dynamic>.from(steps.map((x) => x.toJson())),
        "traffic_speed_entry":
            List<dynamic>.from(trafficSpeedEntry.map((x) => x)),
        "via_waypoint": List<dynamic>.from(viaWaypoint.map((x) => x)),
      };
}

class DistanceBetween {
  String text;
  int value;

  DistanceBetween({
    required this.text,
    required this.value,
  });

  factory DistanceBetween.fromJson(Map<String, dynamic> json) =>
      DistanceBetween(
        text: json["text"],
        value: json["value"],
      );

  Map<String, dynamic> toJson() => {
        "text": text,
        "value": value,
      };
}

class StepDirection {
  DistanceBetween distance;
  DistanceBetween duration;
  Northeast endLocation;
  String htmlInstructions;
  PolylineDirection polyline;
  Northeast startLocation;
  TravelMode travelMode;
  String? maneuver;

  StepDirection({
    required this.distance,
    required this.duration,
    required this.endLocation,
    required this.htmlInstructions,
    required this.polyline,
    required this.startLocation,
    required this.travelMode,
    this.maneuver,
  });

  factory StepDirection.fromJson(Map<String, dynamic> json) => StepDirection(
        distance: DistanceBetween.fromJson(json["distance"]),
        duration: DistanceBetween.fromJson(json["duration"]),
        endLocation: Northeast.fromJson(json["end_location"]),
        htmlInstructions: json["html_instructions"],
        polyline: PolylineDirection.fromJson(json["polyline"]),
        startLocation: Northeast.fromJson(json["start_location"]),
        travelMode: travelModeValues.map[json["travel_mode"]]!,
        maneuver: json["maneuver"],
      );

  Map<String, dynamic> toJson() => {
        "distance": distance.toJson(),
        "duration": duration.toJson(),
        "end_location": endLocation.toJson(),
        "html_instructions": htmlInstructions,
        "polyline": polyline.toJson(),
        "start_location": startLocation.toJson(),
        "travel_mode": travelModeValues.reverse[travelMode],
        "maneuver": maneuver,
      };
}

class PolylineDirection {
  String points;

  PolylineDirection({
    required this.points,
  });

  factory PolylineDirection.fromJson(Map<String, dynamic> json) =>
      PolylineDirection(
        points: json["points"],
      );

  Map<String, dynamic> toJson() => {
        "points": points,
      };
}

enum TravelMode { DRIVING }

final travelModeValues = EnumValues({"DRIVING": TravelMode.DRIVING});

class EnumValues<T> {
  Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    reverseMap = map.map((k, v) => MapEntry(v, k));
    return reverseMap;
  }
}
