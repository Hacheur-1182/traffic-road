import 'package:dio/dio.dart';
import 'package:traffic_road/app/helpers/services/end_point.dart';

class LocationServices {
  Future<Map<String, dynamic>> getDirectionApi() async {
    Dio dio = Dio();
    final response = await dio.get(endPoint);
    Map<String, dynamic> result = {
      "marker_1": response.data['marker_1'],
      "marker_2": response.data['marker_2'],
      "path_1": response.data['path_1'],
      "path_2": response.data['path_2'],
      "bounds_ne": response.data['bounds']['northeast'],
      "bounds_sw": response.data['bounds']['southwest'],
      "distance": response.data['distance'],
      "start_address": response.data['start_address'],
      "end_address": response.data['end_address'],
      "nr_car": response.data['nr_car'],
    };
    // print("result");
    // print(result);
    return result;
  }

  // Future<Map<String, dynamic>> getDirection(
  //     String origin, String destination) async {
  //   Dio dio = Dio();
  //   final url =
  //       "https://maps.googleapis.com/maps/api/directions/json?origin=$origin&destination=$destination&key=$googleApiKey";
  //   final response = await dio.get(url);
  //
  //   if (response.data['status'] == "OK") {
  //     Map<String, dynamic> result = {
  //       "bounds_ne": response.data['routes'][0]['bounds']['northeast'],
  //       "bounds_sw": response.data['routes'][0]['bounds']['southwest'],
  //       "startLocation": response.data['routes'][0]['legs'][0]
  //           ['start_location'],
  //       "endLocation": response.data['routes'][0]['legs'][0]['end_location'],
  //       "polyline": response.data['routes'][0]['overview_polyline']['points'],
  //       "polyline_decoded": PolylinePoints().decodePolyline(
  //           response.data['routes'][0]['overview_polyline']['points']),
  //       "startAddress": response.data['routes'][0]['legs'][0]['start_address'],
  //       "endAddress": response.data['routes'][0]['legs'][0]['end_address'],
  //       "distance": response.data['routes'][0]['legs'][0]['distance']['text'],
  //       "duration": response.data['routes'][0]['legs'][0]['duration']['text'],
  //       "status": response.data['status']
  //     };
  //     return result;
  //   }
  //   Map<String, dynamic> resultError = {
  //     "status": response.data['status'],
  //     "error": "Please enter an existing address, and Retry"
  //   };
  //   return resultError;
  // }

  // Future<DirectionModel> getDirection(String origin, String destination) async {
  //   Dio dio = Dio();
  //   final url =
  //       "https://maps.googleapis.com/maps/api/directions/json?origin=$origin&destination=$destination&key=$googleApiKey";
  //   return await dio.get(url).then(
  //     (response) {
  //       print(response);
  //       return DirectionModel.fromJson(response.data);
  //     },
  //   );
  // }

  // Future<PolylineModel> getRoute() async {
  //   const String url =
  //       "https://routes.googleapis.com/directions/v2:computeRoutes";
  //   Dio dio = Dio();
  //
  //   return await dio
  //       .post(
  //     url,
  //     data: {
  //       "origin": {"address": "RHR2+97X, N 10, Yaoundé"},
  //       "destination": {"address": "RFPV+67 Yaoundé"},
  //       "travelMode": "DRIVE",
  //       "extraComputations": ["TRAFFIC_ON_POLYLINE"],
  //       "routingPreference": "TRAFFIC_AWARE_OPTIMAL"
  //     },
  //     options: Options(
  //       headers: {
  //         'Content-Type': 'application/json',
  //         'X-Goog-Api-Key': googleApiKey,
  //         'X-Goog-FieldMask':
  //             'routes.duration,routes.distanceMeters,routes.routeLabels,routes.polyline',
  //       },
  //     ),
  //   )
  //       .then((response) {
  //     // print(response.data);
  //     return PolylineModel.fromJson(response.data);
  //   });
  // }
}
