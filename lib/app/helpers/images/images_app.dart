class ImageApp{
  static const cameraIcon = "assets/icons/camera.png";
  static const homeIcon = "assets/icons/home2.png";
  static const historyIcon = "assets/icons/screenmirroring.png";
  static const profileIcon = "assets/icons/profilecircle.png";
  static const notificationStatusIcon = "assets/icons/notificationstatus.png";
  static const logoutIcon = "assets/icons/logout.png";
  static const editIcon = "assets/icons/useredit.png";
  static const moreIcon = "assets/icons/more.png";
  static const leftIcon = "assets/icons/left.png";
  static const temperature = "assets/background/group20.png";
  static const humidity = "assets/background/group21.png";
  static const arrowRight = "assets/icons/arrowright.png";
  static const notification = "assets/icons/notification.png";
  static const mais = "assets/background/mais.png";
  static const manioc = "assets/background/manioc.png";
  static const bg = "assets/background/bg.webp";
}