extension NumberExtension on num {
  bool get isInteger => this is int;
  bool get isDecimal => this is! int;
}
