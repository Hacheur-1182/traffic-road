import 'dart:convert';

import 'package:dio/dio.dart' as dio_;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../../helpers/services/end_point.dart';
import '../../../routes/app_pages.dart';



class LoginController extends GetxController {

  final password = TextEditingController();
  final email = TextEditingController();

  final _storage = GetStorage();
  final isLoading = false.obs;

  String? _token;
  String? _email;
  String? _username;

  testLog() {
    print(email);
  }

  List<Map<String, String>> userList = [
    {
      "user_1": "road1@mail.com",
      "password": "road1"
    },
    {
      "user_2": "road2@mail.com",
      "password": "road2"
    }
  ];

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  Future<void> login() async {
    isLoading(true);
    _storage.write('user', true);
    Get.offAndToNamed(AppPages.INITIAL);

    // try {
    //   dio_.Dio dio = dio_.Dio();
    //   print(
    //     "$endPoint/auth",
    //   );
    //   final response = await dio.post(
    //     "$endPoint/auth",
    //     data: {
    //       "email": email.text,
    //       "password": password.text,
    //     },
    //     options: dio_.Options(
    //       headers: {
    //         "ContentType": "application/json",
    //         "Accept": "application/json",
    //       },
    //     ),
    //   );
    //
    //   final responseMessage = response.data["data"];
    //   final responseStatus = response.statusCode;
    //   print(responseMessage);
    //
    //   if (responseStatus == 200) {
    //     _email = responseMessage["email"];
    //     _token = responseMessage["token"];
    //     _username = responseMessage["name"];
    //
    //     final user = jsonEncode({
    //       'username': _username,
    //       'email': _email,
    //       'token': _token,
    //     });
    //     _storage.write('user', user);
    //     // Get.offNamed(AppPages.INITIAL);
    //     Get.offAndToNamed(AppPages.INITIAL);
    //   }
    //   isLoading(false);
    // } on dio_.DioException catch (error) {
    //   print(error.response);
    //   Get.snackbar(
    //     "Error",
    //     error.response == null ? error.response.toString(): error.response.toString(),
    //     colorText: Colors.white,
    //     backgroundColor: CupertinoColors.systemRed,
    //   );
    //   isLoading(false);
    // } catch (e) {
      isLoading(false);
    //   rethrow;
    // }
  }
}
