import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:traffic_road/app/routes/app_pages.dart';

import '../../../helpers/colors/color_app.dart';
import '../../../helpers/fonts/fonts_app.dart';
import '../../components/button_app.dart';
import '../../components/input_form.dart';
import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {
  const LoginView({super.key});

  @override
  Widget build(BuildContext context) {
    final keys = GlobalKey<FormState>();
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 10.0, left: 20.0, right: 20.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              const Text(
                "Login",
                style: TextStyle(
                  fontSize: 40.0,
                  fontFamily: montserratRegular,
                ),
              ),
              const SizedBox(
                height: 20.0,
              ),
              // RichText(text: TextSpan(text: "By signing up you are agreeing our")),
              const Text(
                "By signing up you are agreeing our Term and privacy policy",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 16.0,
                  fontFamily: montserratRegular,
                ),
              ),
              const SizedBox(
                height: 80.0,
              ),
              Form(
                key: keys,
                child: Column(
                  children: [
                    InputFormField(
                      labelText: 'Email Address',
                      obscure: false,
                      controller: controller.email,
                      validatorEntry: (value) {
                        if (value!.isEmpty || !value.contains("@")) {
                          return "invalid email";
                        } else if (value.trim() != "johndoe@traffic.com") {
                          return "Try to sign up";
                        }
                        return null;
                      },
                      saveEntry: (value) {},
                    ),
                    InputFormField(
                      labelText: 'Password',
                      obscure: true,
                      controller: controller.password,
                      validatorEntry: (value) {
                        if (value!.isEmpty) {
                          return "Password is required";
                        } else if (value.trim() != "0123@b456") {
                          return "Wrong password";
                        }
                        return null;
                      },
                      saveEntry: (value) {},
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: const [
                        Text(
                          "Remember password",
                          style: TextStyle(
                            color: Color(0xffD4C5C5),
                            fontSize: 15.0,
                            fontFamily: montserratRegular,
                          ),
                        ),
                        Text(
                          "Forget password",
                          style: TextStyle(
                            color: primaryColor,
                            fontSize: 15.0,
                            fontFamily: montserratRegular,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 20.0,
                    ),
                    ButtonApp(
                      onPressed: () {
                        if (!keys.currentState!.validate()) {
                          return;
                        }
                        keys.currentState!.save();
                        Get.offAndToNamed(AppPages.ROAD_TRAFFIC);
                        // controller.login();
                      },
                      widgetText: Obx(
                        () => controller.isLoading.isTrue
                            ? const CircularProgressIndicator(
                                color: Colors.white,
                              )
                            : const Text(
                                "Login",
                                style: TextStyle(
                                  fontSize: 20.0,
                                  fontFamily: montserratRegular,
                                  color: Colors.white,
                                ),
                              ),
                      ),
                      buttonColor: primaryColor,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
