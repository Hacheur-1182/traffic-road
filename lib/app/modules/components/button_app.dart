import 'package:flutter/material.dart';

class ButtonApp extends StatelessWidget {
  const ButtonApp({
    Key? key,
    required this.onPressed,
    required this.widgetText,
    required this.buttonColor,
  }) : super(key: key);

  final Function() onPressed;
  final Widget widgetText;
  final Color buttonColor;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      width: MediaQuery.of(context).size.width * 0.9,
      decoration: BoxDecoration(
        color: buttonColor,
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: TextButton(
        onPressed: onPressed,
        child: widgetText,
      ),
    );
  }
}