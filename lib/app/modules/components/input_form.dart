import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class InputFormField extends StatelessWidget {
  const InputFormField(
      {super.key,
      required this.labelText,
      required this.obscure,
      this.controller,
      this.saveEntry,
      this.validatorEntry,
      this.textInputType});

  final String labelText;
  final bool obscure;
  final TextEditingController? controller;
  final void Function(String? value)? saveEntry;
  final String? Function(String? value)? validatorEntry;
  final TextInputType? textInputType;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20.0),
      child: TextFormField(
        keyboardType: textInputType,
        obscureText: obscure,
        obscuringCharacter: "*",
        validator: validatorEntry,
        onSaved: saveEntry,
        controller: controller,
        decoration: InputDecoration(
          label: Text(
            labelText,
            style: const TextStyle(
              color: Colors.grey,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(
              color: Color(0xffC4C4C4),
            ),
            borderRadius: BorderRadius.circular(10.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(
              color: Color(0xffC4C4C4),
            ),
            borderRadius: BorderRadius.circular(10.0),
          ),
          errorBorder: OutlineInputBorder(
            borderSide: const BorderSide(
              color: CupertinoColors.systemRed,
            ),
            borderRadius: BorderRadius.circular(10.0),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: const BorderSide(
              color: CupertinoColors.systemPink,
            ),
            borderRadius: BorderRadius.circular(10.0),
          ),
        ),
      ),
    );
  }
}
