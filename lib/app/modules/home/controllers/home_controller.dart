import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../helpers/services/map_route_api.dart';

class HomeController extends GetxController {
  final Completer<GoogleMapController> _controller = Completer();

  final initLat = 3.836060462178594.obs;
  final initLong = 3.836060462178594.obs;

  final isJamPath1 = false.obs;
  final isJamPath2 = false.obs;
  final isRoadworks = false.obs;
  final nrCarPath1 = 0.obs;
  final nrCarPath2 = 0.obs;

  final isLoading = false.obs;
  final isRoute = "path_1".obs;

  Set<Polyline> polyline = <Polyline>{};
  Set<Marker> markers = <Marker>{};

  int _polylineIdCounter = 1;
  int _markersIdCounter = 1;

  // void _setPolyline(List<PointLatLng> points) {
  //   final String polylineValue = 'polyline_$_polylineIdCounter';
  //   _polylineIdCounter++;
  //
  //   polyline.add(
  //     Polyline(
  //       polylineId: PolylineId(polylineValue),
  //       width: 6,
  //       color: Colors.blue,
  //       points: points
  //           .map(
  //             (point) => LatLng(point.latitude, point.longitude),
  //           )
  //           .toList(),
  //     ),
  //   );
  //   update();
  // }

  void _setPolylineData(List<dynamic> list) {
    final String polylineValue = 'polyline_$_polylineIdCounter';
    _polylineIdCounter++;

    // List<Map<String, double>> list = [
    //   {"lat": 3.8442353010655097, "longitude": 11.50137078534783},
    //   {"lat": 3.844473202380726, "longitude": 11.501101689485324},
    //   {"lat": 3.8447164483705194, "longitude": 11.500945689707834},
    //   {"lat": 3.8448457509932723, "longitude": 11.5008087289078},
    //   {"lat": 3.8449748422476517, "longitude": 11.500575603878872},
    //   {"lat": 3.8450619128408476, "longitude": 11.500330064650882},
    //   {"lat": 3.845118582754637, "longitude": 11.500127911346567},
    //   {"lat": 3.845221219586506, "longitude": 11.499859625430162},
    //   {"lat": 3.845358255152064, "longitude": 11.499468909884826},
    //   {"lat": 3.8454227620158434, "longitude": 11.499206267102977},
    //   {"lat": 3.8449721878847214, "longitude": 11.498497076765545},
    //   {"lat": 3.8446489212347927, "longitude": 11.497631433481388},
    //   {"lat": 3.844729375578794, "longitude": 11.496880974166459},
    //   {"lat": 3.8462744221053953, "longitude": 11.494516457234909},
    //   {"lat": 3.845841760250167, "longitude": 11.493153356543246},
    //   {"lat": 3.839931156115469, "longitude": 11.487678320252712},
    //   {"lat": 3.8382021619926974, "longitude": 11.48611013239855},
    //   {"lat": 3.836115938308893, "longitude": 11.484243736573376}
    // ];

    polyline.add(
      Polyline(
        polylineId: PolylineId(polylineValue),
        width: 6,
        startCap: Cap.roundCap,
        endCap: Cap.roundCap,
        color: isRoute.value == "path_1" ? Colors.green : Colors.blue,
        onTap: () {
          print("object");
        },
        points: list
            .map(
              (point) => LatLng(point["lat"]!, point["longitude"]!),
            )
            .toList(),
      ),
    );
    update();
  }

  void _setMarkers(List<LatLng> points, List<String> address) async {
    final String markerValue = 'polyline_$_markersIdCounter';
    _markersIdCounter++;
    for (var point = 0; point < points.length; point++) {
      markers.add(
        Marker(
          markerId: MarkerId("Marker_$markerValue"),
          position: LatLng(points[point].latitude, points[point].longitude),
          infoWindow: InfoWindow(title: address[point]),
        ),
      );
    }
    update();
  }

  // void _setMarkers(List<LatLng> points, List<String> address) async {
  //   final String markerValue = 'polyline_$_markersIdCounter';
  //   _markersIdCounter++;
  //   for (var point = 0; point < points.length; point++) {
  //     markers.add(
  //       Marker(
  //         markerId: MarkerId("Marker_$markerValue"),
  //         position: LatLng(points[point].latitude, points[point].longitude),
  //         infoWindow: InfoWindow(title: address[point]),
  //       ),
  //     );
  //   }
  //   update();
  //   // print("markers inside setMarker");
  //   // print(markers);
  // }

  Future<void> changeDirection(
    double startLat,
    double startLong,
    Map<String, dynamic> boundsNe,
    Map<String, dynamic> boundsSw,
  ) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(startLat, startLong),
          zoom: 12,
        ),
      ),
    );
    controller.animateCamera(
      CameraUpdate.newLatLngBounds(
        LatLngBounds(
          southwest: LatLng(
            boundsSw['lat'],
            boundsSw['lng'],
          ),
          northeast: LatLng(
            boundsNe['lat'],
            boundsNe['lng'],
          ),
        ),
        25,
      ),
    );
  }

  // Future<void> goToPlace(
  //   double startLat,
  //   double startLong,
  //   double endLat,
  //   double endLong,
  //   Map<String, dynamic> boundsNe,
  //   Map<String, dynamic> boundsSw,
  // ) async {
  //   final GoogleMapController controller = await _controller.future;
  //   controller.animateCamera(
  //     CameraUpdate.newCameraPosition(
  //       CameraPosition(
  //         target: LatLng(startLat, startLong),
  //         zoom: 12,
  //       ),
  //     ),
  //   );
  //
  //   controller.animateCamera(
  //     CameraUpdate.newLatLngBounds(
  //       LatLngBounds(
  //         southwest: LatLng(
  //           boundsSw['lat'],
  //           boundsSw['lat'],
  //         ),
  //         northeast: LatLng(
  //           boundsNe['lat'],
  //           boundsNe['lng'],
  //         ),
  //       ),
  //       25,
  //     ),
  //   );
  //
  //   // _setMarkers([LatLng(startLat, startLong), LatLng(endLat, endLong)]);
  //   // print("markers on goToPlace");
  //   // print(markers);
  // }

  final count = 0.obs;
  @override
  void onInit() {
    getAnalyze("path_1");
    super.onInit();
  }

  @override
  void onReady() {
    if (kDebugMode) {
      print("onReady");
    }
    super.onReady();
  }

  @override
  void onClose() {
    if (kDebugMode) {
      print("onClose");
    }
    super.onClose();
  }

  getAnalyze(String path) async {
    isLoading(true);

    try {
      final response = await LocationServices().getDirectionApi();

      initLat(response["marker_2"]["lat"]);
      initLong(response["marker_2"]["longitude"]);

      _setMarkers([
        LatLng(
          response["marker_1"]["lat"],
          response["marker_1"]["longitude"],
        ),
        LatLng(
          response["marker_2"]["lat"],
          response["marker_2"]["longitude"],
        ),
      ], [
        response["start_address"],
        response["end_address"],
      ]);

      if (path == "path_1") {
        isJamPath1(response['path_1']['is_jam']);
        // print("response['path_1']");
        // print(response['path_1']["path"]);
        isRoadworks(response['path_1']['is_roadworks']);
        nrCarPath1(response['path_1']['nr_car']);

        _setPolylineData(response["path_1"]['path']);
        changeDirection(
          response["marker_1"]["lat"],
          response["marker_1"]["longitude"],
          response["bounds_ne"],
          response["bounds_sw"],
        );
      } else {
        isJamPath2(response['path_2']['is_jam']);
        isRoadworks(response['path_2']['is_roadworks']);
        nrCarPath2(response['path_2']['nr_car']);

        _setPolylineData(response["path_2"]['path']);
        changeDirection(
          response["marker_2"]["lat"],
          response["marker_2"]["longitude"],
          response["bounds_ne"],
          response["bounds_sw"],
        );
      }
    } catch (error) {
      Get.snackbar(
        "Not Found",
        "Please enter an existing address, and Retry",
        colorText: Colors.white,
        backgroundColor: Colors.red,
        duration: const Duration(seconds: 6),
      );
      if (kDebugMode) {
        print(error);
      }
    }
    isLoading(false);
  }

  // getRoadDirection() async {
  //   isLoading(true);
  //
  //   try {
  //     final response = await LocationServices().getDirection(
  //       Get.arguments['from'],
  //       Get.arguments['to'],
  //     );
  //
  //     initLat = response["startLocation"]["lat"];
  //     initLong = response["startLocation"]["lng"];
  //
  //     goToPlace(
  //       response["startLocation"]["lat"],
  //       response["startLocation"]["lng"],
  //       response["endLocation"]["lat"],
  //       response["endLocation"]["lng"],
  //       response["bounds_ne"],
  //       response["bounds_sw"],
  //     );
  //     // _setPolyline(response["polyline_decoded"]);
  //     _setPolylineData();
  //     // _setPolyline(response["polyline_decoded"]);
  //     _setMarkers([
  //       LatLng(
  //         response["startLocation"]["lat"],
  //         response["startLocation"]["lng"],
  //       ),
  //       LatLng(
  //         response["endLocation"]["lat"],
  //         response["endLocation"]["lng"],
  //       ),
  //     ], [
  //       response["startAddress"],
  //       response["endAddress"],
  //     ]);
  //   } catch (error) {
  //     Get.snackbar(
  //       "Not Found",
  //       "Please enter an existing address, and Retry",
  //       colorText: Colors.white,
  //       backgroundColor: Colors.red,
  //       duration: const Duration(seconds: 6),
  //     );
  //     print(error);
  //   }
  //   isLoading(false);
  // }
}
