import 'dart:async';

import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:traffic_road/app/helpers/fonts/fonts_app.dart';
import 'package:traffic_road/app/routes/app_pages.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final Completer<GoogleMapController> controllerG = Completer();

    return GetBuilder(
      init: HomeController(),
      builder: (controller) {
        return Scaffold(
          body: Obx(
            () => controller.isLoading.value
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : Stack(
                    children: [
                      GoogleMap(
                        mapType: MapType.normal,
                        initialCameraPosition: CameraPosition(
                          target: LatLng(
                            controller.initLat.value,
                            controller.initLong.value,
                          ),
                          zoom: 14.4,
                        ),
                        // onMapCreated: (GoogleMapController ctrl) {
                        //   controllerG.complete(ctrl);
                        // },
                        markers: controller.markers,
                        polylines: controller.polyline,
                      ),
                      Positioned(
                        right: 1,
                        bottom: 100,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            IconButton(
                              onPressed: () {
                                controller.isRoute("path_1");
                                controller.getAnalyze("path_1");
                              },
                              icon: const CircleAvatar(
                                backgroundColor: Colors.green,
                                child: Center(
                                  child: Text(
                                    "R1",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: montserratBold,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            IconButton(
                              onPressed: () {
                                controller.isRoute("path_2");
                                controller.getAnalyze("path_2");
                              },
                              icon: const CircleAvatar(
                                child: Center(
                                  child: Text(
                                    "R2",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: montserratBold,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Positioned(
                        left: 15,
                        bottom: 100,
                        child: TextButton(
                          style: TextButton.styleFrom(
                            backgroundColor: Colors.blueAccent,
                          ),
                          onPressed: () {
                            Get.offAndToNamed(AppPages.INITIAL);
                          },
                          child: const Text(
                            "Reload",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                      controller.isLoading.value
                          ? const CircularProgressIndicator()
                          : Padding(
                              padding: const EdgeInsets.only(
                                top: 50.0,
                                left: 20.0,
                                right: 20.0,
                              ),
                              child: Container(
                                height: 150,
                                width: 400,
                                padding: const EdgeInsets.all(20.0),
                                decoration: BoxDecoration(
                                  color: Colors.white.withOpacity(.8),
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                                child: Obx(
                                  () => Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: const [
                                          Text(
                                            "Route 1",
                                            style: TextStyle(
                                              color: Colors.green,
                                              fontFamily: montserratBold,
                                            ),
                                          ),
                                          Text(
                                            "Route 2",
                                            style: TextStyle(
                                              color: Colors.blue,
                                              fontFamily: montserratBold,
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 10.0,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          RichText(
                                            text: TextSpan(
                                              children: [
                                                const TextSpan(
                                                  text: "Embouteillage:",
                                                  style: TextStyle(
                                                    color: Colors.black,
                                                    fontFamily: montserratLight,
                                                  ),
                                                ),
                                                TextSpan(
                                                  text: controller
                                                          .isJamPath1.value
                                                      ? " Oui"
                                                      : " Non",
                                                  style: const TextStyle(
                                                    color: Colors.green,
                                                    fontFamily: montserratBold,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          RichText(
                                            text: TextSpan(
                                              children: [
                                                const TextSpan(
                                                  text: "Embouteillage:",
                                                  style: TextStyle(
                                                    color: Colors.black,
                                                    fontFamily: montserratLight,
                                                  ),
                                                ),
                                                TextSpan(
                                                  text: controller
                                                          .isJamPath2.value
                                                      ? " Oui"
                                                      : " Non",
                                                  style: const TextStyle(
                                                    color: Colors.blue,
                                                    fontFamily: montserratBold,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 8.0,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          RichText(
                                            text: TextSpan(
                                              children: [
                                                const TextSpan(
                                                  text: "Travaux:",
                                                  style: TextStyle(
                                                    color: Colors.black,
                                                    fontFamily: montserratLight,
                                                  ),
                                                ),
                                                TextSpan(
                                                  text: controller
                                                          .isRoadworks.value
                                                      ? " Oui"
                                                      : " Non",
                                                  style: const TextStyle(
                                                    color: Colors.green,
                                                    fontFamily: montserratBold,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                          RichText(
                                            text: TextSpan(
                                              children: [
                                                const TextSpan(
                                                  text: "Travaux:",
                                                  style: TextStyle(
                                                    color: Colors.black,
                                                    fontFamily: montserratLight,
                                                  ),
                                                ),
                                                TextSpan(
                                                  text: controller
                                                          .isRoadworks.value
                                                      ? " Oui"
                                                      : " Non",
                                                  style: const TextStyle(
                                                    color: Colors.blue,
                                                    fontFamily: montserratBold,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 8.0,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          RichText(
                                            text: TextSpan(
                                              children: [
                                                const TextSpan(
                                                  text: "Nbres de voitures:",
                                                  style: TextStyle(
                                                    color: Colors.black,
                                                    fontFamily: montserratLight,
                                                  ),
                                                ),
                                                TextSpan(
                                                  text:
                                                      " ${controller.nrCarPath1}",
                                                  style: const TextStyle(
                                                    color: Colors.green,
                                                    fontFamily: montserratBold,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                          RichText(
                                            text: TextSpan(
                                              children: [
                                                const TextSpan(
                                                  text: "Nbres de voitures:",
                                                  style: TextStyle(
                                                    color: Colors.black,
                                                    fontFamily: montserratLight,
                                                  ),
                                                ),
                                                TextSpan(
                                                  text:
                                                      " ${controller.nrCarPath2}",
                                                  style: const TextStyle(
                                                    color: Colors.blue,
                                                    fontFamily: montserratBold,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                    ],
                  ),
          ),
        );
      },
    );
  }
}
