import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:traffic_road/app/helpers/colors/color_app.dart';

class MapTraffic extends StatefulWidget {
  const MapTraffic({super.key});

  @override
  State<MapTraffic> createState() => _MapTrafficState();
}

class _MapTrafficState extends State<MapTraffic> {
  Set<Polyline> _polylines = Set<Polyline>();
  Set<Marker> _markers = Set<Marker>();

  int _polylineIdCounter = 1;

  void _setPolyline(List<PointLatLng> points) {
    final String polylineValue = 'polyline_$_polylineIdCounter';
    _polylineIdCounter++;

    _polylines.add(
      Polyline(
        polylineId: PolylineId(polylineValue),
        width: 2,
        color: primaryColor,
        points: points
            .map(
              (point) => LatLng(point.latitude, point.longitude),
            )
            .toList(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text('HomeView'),
      //   centerTitle: true,
      // ),
      body: GoogleMap(
        mapType: MapType.normal,
        initialCameraPosition: const CameraPosition(
          target: LatLng(3.8408833, 11.5487853),
          zoom: 16,
        ),
        markers: {
          const Marker(
            markerId: MarkerId("Fontana"),
            position: LatLng(3.8408833, 11.5487853),
          )
        },
        polylines: _polylines,
      ),
    );
  }
}
