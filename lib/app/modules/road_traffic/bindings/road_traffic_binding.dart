import 'package:get/get.dart';

import '../controllers/road_traffic_controller.dart';

class RoadTrafficBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RoadTrafficController>(
      () => RoadTrafficController(),
    );
  }
}
