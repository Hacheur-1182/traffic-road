import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:traffic_road/app/routes/app_pages.dart';

class RoadTrafficController extends GetxController {
  final keyForm = GlobalKey<FormState>();
  final isLoading = false.obs;
  final textError = ''.obs;

  TextEditingController fromText = TextEditingController();
  TextEditingController toText = TextEditingController();

  void onSubmit() async {
    if (!keyForm.currentState!.validate()) {
      return;
    }
    keyForm.currentState!.save();

    isLoading(true);

    Get.toNamed(
      AppPages.INITIAL,
      arguments: {
        "from": fromText.text,
        "to": toText.text,
      },
    );

    // final response = await LocationServices().getDirection(
    //   fromText.text,
    //   toText.text,
    // );
    //
    // if (response['status'] == "OK") {
    //   Get.toNamed(
    //     AppPages.INITIAL,
    //     arguments: {
    //       "from": fromText.text,
    //       "to": toText.text,
    //     },
    //   );
    // } else {
    //   Get.snackbar(
    //     "Not Found",
    //     "Please enter an existing address, and Retry",
    //     colorText: Colors.white,
    //     backgroundColor: Colors.red,
    //     margin: const EdgeInsets.only(top: 20, left: 10.0, right: 20.0),
    //     duration: const Duration(seconds: 6),
    //   );
    //   isLoading(false);
    // }
    Future.delayed(const Duration(seconds: 2), () {
      isLoading(false);
    });
  }

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
