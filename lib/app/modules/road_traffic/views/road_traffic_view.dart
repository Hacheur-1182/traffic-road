import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:traffic_road/app/modules/components/button_app.dart';
import 'package:traffic_road/app/modules/components/input_form.dart';

import '../../../helpers/fonts/fonts_app.dart';
import '../controllers/road_traffic_controller.dart';

class RoadTrafficView extends GetView<RoadTrafficController> {
  const RoadTrafficView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Analyze Traffic Road',
          style: TextStyle(
            fontFamily: montserratSemiBold,
          ),
        ),
        centerTitle: true,
        elevation: 0.4,
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Padding(
          padding: const EdgeInsets.only(top: 100.0, left: 20.0, right: 20.0),
          child: Form(
            key: controller.keyForm,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              // mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  'Select Your destination and origin',
                  style: TextStyle(
                    fontFamily: montserratRegular,
                    fontSize: 16,
                    decoration: TextDecoration.underline,
                  ),
                ),
                const SizedBox(
                  height: 20.0,
                ),
                // Obx(
                //   () => controller.textError.isEmpty
                //       ? const SizedBox(
                //           height: 0.0,
                //         )
                //       : Container(
                //           height: 50,
                //           margin: const EdgeInsets.only(bottom: 20.0),
                //           alignment: Alignment.center,
                //           decoration: BoxDecoration(
                //             color: Colors.red,
                //             borderRadius: BorderRadius.circular(10.0),
                //           ),
                //           child: Text(
                //             controller.textError.toString(),
                //             style: const TextStyle(
                //               color: Colors.white,
                //               fontFamily: montserratSemiBold,
                //               fontSize: 12,
                //             ),
                //           ),
                //         ),
                // ),
                InputFormField(
                  labelText: "From",
                  obscure: false,
                  controller: controller.fromText,
                  validatorEntry: (from) {
                    if (from!.isEmpty) {
                      return "This field can't be empty";
                    } else if (from.toLowerCase().trim() != "carrefour vogt") {
                      return "The right destination is carrefour vogt";
                    }
                    return null;
                  },
                ),
                InputFormField(
                    labelText: "To",
                    obscure: false,
                    controller: controller.toText,
                    validatorEntry: (to) {
                      if (to!.isEmpty) {
                        return "This field can't be empty";
                      } else if (to.toLowerCase().trim() != "rond point express") {
                        return "The right destination is rond point express";
                      }
                      return null;
                    }),
                ButtonApp(
                  onPressed: controller.onSubmit,
                  widgetText: Obx(
                    () => controller.isLoading.value
                        ? const CircularProgressIndicator(
                            color: Colors.white,
                          )
                        : const Text(
                            "Analyze",
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: montserratSemiBold,
                            ),
                          ),
                  ),
                  buttonColor: Colors.blueAccent,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
