import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/error_connection_controller.dart';

class ErrorConnectionView extends GetView<ErrorConnectionController> {
  const ErrorConnectionView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('ErrorConnectionView'),
        centerTitle: true,
      ),
      body: const Center(
        child: Text(
          'ErrorConnectionView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
