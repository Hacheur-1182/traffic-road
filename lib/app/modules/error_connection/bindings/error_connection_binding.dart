import 'package:get/get.dart';

import '../controllers/error_connection_controller.dart';

class ErrorConnectionBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ErrorConnectionController>(
      () => ErrorConnectionController(),
    );
  }
}
